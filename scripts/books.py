import requests
import json
from bs4 import BeautifulSoup

# Send a request to the website and retrieve the HTML content
response = requests.get('https://books.toscrape.com/')
html = response.text

# Parse the HTML content and create a BeautifulSoup object
soup = BeautifulSoup(html, 'html.parser')

# Find all the book titles on the page
titles = soup.find_all('h3')

# Create a list to store the book data
books = []

# Extract the data for each book
for title in titles:
    book = {}
    book['title'] = title.text
    book['link'] = title.find('a')['href']
    books.append(book)

# Write the book data to a JSON file
with open('books.json', 'w') as outfile:
    json.dump(books, outfile)
