# import requests
# from bs4 import BeautifulSoup

# # Send a request to the website and retrieve the HTML content
# response = requests.get('http://nzdworld.com/')
# html = response.text

# # Parse the HTML content and create a BeautifulSoup object
# soup = BeautifulSoup(html, 'html.parser')

# # Find all the elements with the "title" attribute
# elements = soup.find_all(title=True)

# # Print the tag and title of each element
# for element in elements:
#     print(f'Tag: {element.name} Title: {element["title"]}')

import requests
from bs4 import BeautifulSoup
import json


# Send a request to the website and retrieve the HTML content
response = requests.get('http://nzdworld.com/')
html = response.text

# Parse the HTML content and create a BeautifulSoup object
soup = BeautifulSoup(html, 'html.parser')

movie = []

# Find the ul element with the class "g1-collection-items"
ul = soup.find('ul', class_='g1-collection-items')

# Find all the movie titles within the ul element
elements = ul.find_all('a', title=True)

# Print the text of each title
for element in elements:
    # print(f'Tag: {element.name} Title: {element["title"]} LINK: {element["href"]}')
    response = requests.get(element["href"])
    html = response.text
    soup = BeautifulSoup(html, 'html.parser')
    movieName = soup.find('h1', class_= 'g1-mega g1-mega-1st entry-title')
    # print(movieName.text)

    links = soup.find_all('a')
    downloadLink = []
    for link in links:
        if 'DOWNLOAD' in link.text:
            downloadLink.append(link['href'])
        if 'Episode' in link.text:
            downloadLink.append(link['href'])

    # print(downloadLink)

    movieObj = {}
    movieObj['movieName'] = movieName.text
    movieObj['movieLink'] = element['href']
    movieObj['downloadLinks'] = downloadLink

    movie.append(movieObj)

# print(movie)


# with open('movies.json', 'w') as outfile:
#     json.dump(movie, outfile)

with open('movies.json', 'w') as outfile:
    json.dump(movie, outfile, indent=4)

# print('Collected {movie.}')
print(f'Total number of movies: {len(movie)}')
